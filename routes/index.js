var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'Express',
    subtitle: 'DevOps con Gitlab'
  });
});

router.get("/hello-world", function(req, res){
  res.send("hello world!");
});

module.exports = router;
